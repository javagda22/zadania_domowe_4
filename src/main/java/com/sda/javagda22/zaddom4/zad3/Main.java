package com.sda.javagda22.zaddom4.zad3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj linię tekstu:");
        String tekst = scanner.nextLine();

        tekst = tekst.replace(",", ""); // zastępujemy znaki , (usuwamy je)
        tekst = tekst.replace(".", ""); // zastępujemy znaki , (usuwamy je)

        tekst = tekst.toLowerCase(); // zamiana na małe litery

        //ala | lubi | koty | ale | ala nie jest przez koty lubiana
//        String[] slowa = tekst.split(" ", 5); //
        // ala | lubi | koty | ale | ala | nie | jest | przez | koty | lubiana
        String[] slowa = tekst.split(" ");

        for (int i = 0; i < slowa.length; i++) {
            String slowoKtoregoSzukam = slowa[i]; // ala, lubi, koty...

            // pętla wewnętrzna iteruje przez tablicę słów i sprawdza ilość wystąpień
            // podanego słowa w tablicy.
            int licznikWystapien = 0;
            for (int j = 0; j < slowa.length; j++) {
                if (slowa[j].equals(slowoKtoregoSzukam)) {
                    licznikWystapien++;
                }
            }

            System.out.println(slowoKtoregoSzukam + " -> " + licznikWystapien);
        }

    }
}
