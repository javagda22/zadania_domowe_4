package com.sda.javagda22.zaddom4.zad3;

import java.util.Scanner;

public class Main_UnikalneZliczenia {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj linię tekstu:");
        String tekst = scanner.nextLine();

        tekst = tekst.replace(",", ""); // zastępujemy znaki , (usuwamy je)
        tekst = tekst.replace(".", ""); // zastępujemy znaki , (usuwamy je)

        tekst = tekst.toLowerCase(); // zamiana na małe litery

        //ala | lubi | koty | ale | ala nie jest przez koty lubiana
//        String[] slowa = tekst.split(" ", 5); // < - można coś takiego zrobić

        // ala | lubi | koty | ale | ala | nie | jest | przez | koty | lubiana
        String[] slowa = tekst.split(" "); // brak ograniczenia

        for (int i = 0; i < slowa.length; i++) {
            String slowoKtoregoSzukam = slowa[i]; // ala, lubi, koty...

            // pętla wewnętrzna iteruje przez tablicę słów i sprawdza ilość wystąpień
            // podanego słowa w tablicy.
            boolean czyJuzWystapilo = false;
            // zmienna czyJuzWystapilo bedzie kontrolować wypisanie komunikatu na ekran

            int licznikWystapien = 0;
            for (int j = 0; j < slowa.length; j++) {
                if (slowa[j].equals(slowoKtoregoSzukam)) {
                    // przerywam pętlę jeśli szukane słowo wystąpiło wcześniej
                    // niż obecne wystapienie
                    if (j < i) {
                        czyJuzWystapilo = true;
                        break;
                    }
                    licznikWystapien++;
                }
            }

            // wypisuję na ekran tylko jeśli szukane słowo jeszcze wcześniej nie było wypisane
            if (!czyJuzWystapilo) {
                System.out.println(slowoKtoregoSzukam + " -> " + licznikWystapien);
            }
        }

    }
}
