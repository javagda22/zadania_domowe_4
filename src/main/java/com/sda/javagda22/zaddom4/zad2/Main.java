package com.sda.javagda22.zaddom4.zad2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj linię tekstu:");
        String tekst = scanner.nextLine();

        System.out.println(tekst.toLowerCase()); // zamiana na małe litery
        System.out.println(tekst.toUpperCase()); // zamiana na duże litery
    }
}
