package com.sda.javagda22.zaddom4.zad8_obiekt;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        final Robot robot = new Robot("Marian");
        Robot robot2 = new Robot("Rafał");

        Scanner scanner = new Scanner(System.in);

        String komenda;
        do {
            komenda = scanner.next();

            if (komenda.equals("ruch")) {
                // potrzebujemy wczytać jaki ruch mamy wykonać
                String jakiRuchWykonac = scanner.next(); // wczytuję jedno słowo

                // string - > enum
                // w linii poleceń wpisałem "KROK_LEWA" <= String!
                //
//                RuchRobota ruch = jakiRuchWykonac; // String -> enum
                RuchRobota ruch = RuchRobota.valueOf(jakiRuchWykonac); // String -> enum
//                switch (jakiRuchWykonac) { < można zastąpić
//                    case "KROK_LEWA":
//                      robot.poruszRobotem(RuchRobota.KROK_LEWA);
//                    case "KROK_PRAWA":
//                      robot.poruszRobotem(RuchRobota.KROK_PRAWA);
//                    case "RUCH_REKA_LEWA":
//                      ...
//                    case "RUCH_REKA_PRAWA":
//                      ...
//                    case "SKOK":
//                      ...
//                }
                RuchRobota ruchDoWykonania = RuchRobota.valueOf(jakiRuchWykonac);

                robot.poruszRobotem(ruchDoWykonania);
            } else if (komenda.equals("naladuj")) {

                robot.naladujRobota();
            } else if (komenda.equals("wlacz")) {

                robot.wlaczRobota();
            } else if (komenda.equals("wylacz")) {

                robot.wylaczRobota();
            }else if(komenda.equals("spr1")){
                robot.sprawdzDoIluNaladowac();
            }else if(komenda.equals("spr2")){

                robot2.sprawdzDoIluNaladowac();
            }

//            robot = new Robot("Inny Marian");
//            robot2 = new Robot("Inny Marian");

            // wykonuj dopóki komenda nie jest równa "quit"
        } while (!komenda.equals("quit"));
    }
}
