package com.sda.javagda22.zaddom4.zad8_obiekt;

public class Robot {
    private final static int DO_ILU_NALADOWAC = 90;

    private int poziomBaterii;
    private String nazwaRobota;
    private boolean czyWlaczony;

    public Robot(String nazwaRobota) {
        this.nazwaRobota = nazwaRobota;
        this.poziomBaterii = DO_ILU_NALADOWAC;
        this.czyWlaczony = false;
    }

    public void poruszRobotem(RuchRobota ruchRobota) {
        if (czyWlaczony) {
            // możemy zrobić ruch
            if (poziomBaterii >= ruchRobota.getPotrzebnaIloscBaterii()) {
                poziomBaterii -= ruchRobota.getPotrzebnaIloscBaterii();
                // wypisuję jaki ruch wykonałem
                System.out.println("Ruch robota : " + ruchRobota + " poziom baterii: " + poziomBaterii);
            } else {
                System.out.println("Niedostateczna ilość baterii");
            }
        } else {
            System.out.println("Robot wyłączony.");
        }
    }

    public void sprawdzDoIluNaladowac() {
        System.out.println(nazwaRobota + " mówi: " + DO_ILU_NALADOWAC);
//        DO_ILU_NALADOWAC -= 1;
//        DO_ILU_NALADOWAC <- zmienia wartość 'podmienia się obiekt'
    }

    public void naladujRobota() {
        poziomBaterii = DO_ILU_NALADOWAC;
        System.out.println("Robot został naładowany");
    }

    public void wlaczRobota() {
        czyWlaczony = true;
        System.out.println("Robot został włączony");

    }

    public void wylaczRobota() {
        czyWlaczony = false;
        System.out.println("Robot został wyłączony");
    }
}
