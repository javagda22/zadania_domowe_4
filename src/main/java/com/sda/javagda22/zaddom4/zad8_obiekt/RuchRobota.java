package com.sda.javagda22.zaddom4.zad8_obiekt;

public enum RuchRobota {
    KROK_LEWA(15),
    KROK_PRAWA(15),
    RUCH_REKA_LEWA(5),
    RUCH_REKA_PRAWA(5),
    OBROT_W_PRZOD(50),
    SKOK(30);

    private int potrzebnaIloscBaterii;

    RuchRobota(int potrzebnaIloscBaterii) {
        this.potrzebnaIloscBaterii = potrzebnaIloscBaterii;
    }

    public int getPotrzebnaIloscBaterii() {
        return potrzebnaIloscBaterii;
    }
}
