package com.sda.javagda22.zaddom4.zad1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj linię tekstu:");
        String tekst = scanner.nextLine();

        System.out.println(tekst.replace(", ", " makarena "));
    }
}
