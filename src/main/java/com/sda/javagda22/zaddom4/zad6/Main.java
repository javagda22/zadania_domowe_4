package com.sda.javagda22.zaddom4.zad6;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj słowo:");
        String tekst = scanner.next();

        char[] tablica = tekst.toCharArray();

        String odwroconeSlowo = "";

        // spoko rozwiązanie które wypisuje tekst w zadanym formacie
        for (int i = tablica.length - 1; i >= 0; i--) {
            System.out.print(tablica[i]);

            odwroconeSlowo += tablica[i];
            // dopisujemy kolejny znak.
        }
        System.out.println();
        System.out.println(odwroconeSlowo);

        // podejście StringBuilder
        StringBuilder stringBuilder = new StringBuilder(tekst);
        String odworoconeStringBuilderem = stringBuilder.reverse().toString();
    }
}
