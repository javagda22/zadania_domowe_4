package com.sda.javagda22.zaddom4.zad7;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj linię tekstu:");
        String tekst = scanner.nextLine();

        //2 * (3.4 - (-7)/2)*(a-2)/(b-1)))

        // (2+3)
        // )2+3(

        int iloscLewychNawiasów = 0;
        int iloscPrawychNawiasow = 0;

        for (int i = 0; i < tekst.length(); i++) {
            if (tekst.charAt(i) == '(') {
                iloscLewychNawiasów++;
            }
            if (tekst.charAt(i) == ')') {
                iloscPrawychNawiasow++;
            }

            if (iloscLewychNawiasów < iloscPrawychNawiasow) {
                // nawias został domknięty nie będąc otwartym
                // ilość nawiasów prawych jest zbyt duża, domknięcie nie otwartego nawiasu
                break; // <-- przerywam pętle w momencie kiedy wiemy że sparowanie jest złe.
            }
        }

        if (iloscLewychNawiasów != iloscPrawychNawiasow) {
            System.out.println("Błędne sparowanie nawiasów!");
        } else {
            System.out.println("Poprawne parowanie nawiasów");
        }
    }
}
