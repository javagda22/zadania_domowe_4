package com.sda.javagda22.zaddom4.zad4;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Cześć użytkowniku! Jestem prostym kalkulatorem! \n" +
                "Dozwolone operacje, to operacje: \n" +
                " -> dodawania \n" +
                " -> odejmowania \n" +
                " -> mnożenia \n" +
                " -> dzielenia (nie przez zero!) \n");

        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj pierwszą liczbę:");
        double liczba1 = scanner.nextDouble();

        System.out.println("Podaj działanie matematyczne (+,-,/,*) :");
        String dzialanie = scanner.next();

        System.out.println("Podaj drugą liczbę:");
        double liczba2 = scanner.nextDouble();

//        switch (dzialanie) {
//            case "+":
//                System.out.println("Wynik: " + (liczba1 + liczba2));
//                break;
//            case "-":
//                System.out.println("Wynik: " + (liczba1 - liczba2));
//                break;
//            case "/":
//                if (liczba2 == 0.0) {
//                    System.out.println("Pamiętaj cholero, by nie dzielić przez 0!");
//                    break;
//                }
//                System.out.println("Wynik: " + (liczba1 / liczba2));
//                break;
//            case "*":
//                System.out.println("Wynik: " + (liczba1 * liczba2));
//                break;
//            default:
//                System.out.println("Nie rozpoznano działania matematycznego!");
//        }

//        if (dzialanie.equals("+")) { // <- odwracanie
        if ("+".equals(dzialanie)) {
            System.out.println("Wynik: " + (liczba1 + liczba2));
        } else if ("-".equals(dzialanie)) {
            System.out.println("Wynik: " + (liczba1 - liczba2));
        } else if ("/".equals(dzialanie)) {
            if (liczba2 == 0.0) {
                System.out.println("Pamiętaj cholero, by nie dzielić przez 0!");
            } else {
                System.out.println("Wynik: " + (liczba1 / liczba2));
            }
        } else if ("*".equals(dzialanie)) {
            System.out.println("Wynik: " + (liczba1 * liczba2));
        } else {
            System.out.println("Nie rozpoznano działania matematycznego!");
        }
    }
}
