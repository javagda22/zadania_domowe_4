package com.sda.javagda22.zaddom4.zad5;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj słowo:");
        String tekst = scanner.next();

        char[] literki = tekst.toCharArray();

        // 5
        // 0, 1, 2, 3, 4
        // 10
        // 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
        char ostatniZnak = literki[literki.length - 1]; // ostatni znak!
//        char ostatniZnak = literki[tekst.length() - 1]; // ostatni znak!

        int iloscWystapien = 0;
        for (int i = 0; i < literki.length; i++) {
            if (literki[i] == ostatniZnak) {
                iloscWystapien++;
            }
        }
        System.out.println("Literka " + ostatniZnak + " występuje " + iloscWystapien + " razy");


    }
}
